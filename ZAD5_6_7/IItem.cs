using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD5_6_7
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
