using System;

namespace ZAD5_6_7
{
    class Entry
    {
        static void Main(string[] args)
        {
            //ZAD 5 
            DVD dvd = new DVD("Annihilation", DVDType.MOVIE, 150);
            VHS vhs = new VHS("Tarzan", 60);
            Book book = new Book("Ostri predmeti", 120);
            BuyVisitor buyvisitor = new BuyVisitor();

            Console.WriteLine(dvd.Accept(buyvisitor));
            Console.WriteLine(vhs.Accept(buyvisitor));
            Console.WriteLine(book.Accept(buyvisitor));
            Console.WriteLine();
            //ZAD 6 
            RentVisitor rentVisitor = new RentVisitor();
            DVD dvdsoftware = new DVD("Kaspersky", DVDType.SOFTWARE, 350);
            double price = dvdsoftware.Accept(rentVisitor);
            if (double.IsNaN(price))
            {
                Console.WriteLine("DVD se ne moze posuditi");
            }
            else
                Console.WriteLine("Cijena posudenog DVD-a : " + price);
            
            Console.WriteLine(vhs.Accept(rentVisitor));
            Console.WriteLine(book.Accept(rentVisitor));
            //ZAD 7 
            Cart cart = new Cart();
            cart.AddItem(dvd);
            cart.AddItem(vhs);
            cart.AddItem(book);
            Console.WriteLine("Ukupna cijena kosarice : " + cart.Accept(rentVisitor));
        }
    }
}
