using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD5_6_7
{
    class Cart : IItem
    {
        private List<IItem> items = new List<IItem>();

        public double Accept(IVisitor visitor)
        {
            double sumprice = 0;
            for(int i = 0; i < items.Count; i++)
            {
                sumprice += items[i].Accept(visitor);
            }
            return sumprice;
        }

        public void AddItem(IItem item)
        {
            items.Add(item);
        }
        public void RemoveItem(IItem item) 
        {
            items.Remove(item);
        }

    }
}
