using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD5_6_7
{
    class RentVisitor : IVisitor
    {
        private const double Tax = 0.1;
        public double Visit(DVD DVDItem)
        {
            /* ZAD 6
             * if(DVDItem.Type == DVDType.SOFTWARE)
            {
                return double.NaN;
            }*/
             if(DVDItem.Type == DVDType.SOFTWARE)
            {
                return DVDItem.Price;
            }else
            return DVDItem.Price * (1 + Tax);
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * (1 + Tax);
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price * (1 + Tax);
        }
    }
}
