using System;

namespace ZAD1_2
{
    class Entry
    {
        static void Main(string[] args)
        {
            //ZAD 1

            double[] array = { 4, 1, 3, 10, 2, 6, 7, 5, 8, 9 };
            NumberSequence numbersequence1 = new NumberSequence(array);
            NumberSequence numbersequence2 = new NumberSequence(array);
            NumberSequence numbersequence3 = new NumberSequence(array);

            Console.Write(numbersequence1.ToString());
            BubbleSort bubbleSort = new BubbleSort();
            SequentialSort sequentialSort = new SequentialSort();
            CombSort combSort = new CombSort();
            Console.WriteLine();
            numbersequence1.SetSortStrategy(bubbleSort);
            numbersequence1.Sort();
            Console.WriteLine("Bubble sort : ");
            Console.Write(numbersequence1.ToString());
            Console.WriteLine();
            numbersequence2.SetSortStrategy(sequentialSort);
            numbersequence2.Sort();
            Console.WriteLine("Sequential sort : ");
            Console.Write(numbersequence2.ToString());
            Console.WriteLine();
            numbersequence3.SetSortStrategy(combSort);
            numbersequence3.Sort();
            Console.WriteLine("Comb sort : ");
            Console.Write(numbersequence3.ToString());

            //ZAD 2 
            double numberToSearch;
            LinearSearch linearSearch = new LinearSearch();
            numbersequence1.SetSearchStrategy(linearSearch);
            if (double.TryParse(Console.ReadLine(), out numberToSearch))
            {
                int index = numbersequence1.Search(numberToSearch);
                if (index >= 0)
                {
                    Console.WriteLine("Element se nalazi na indeksu : " + index.ToString());
                }else
                Console.WriteLine("Element se ne nalazi u polju");
            }

        }
    }
}
