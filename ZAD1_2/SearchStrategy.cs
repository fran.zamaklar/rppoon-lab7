using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1_2
{
    abstract class SearchStrategy
    {
        public abstract int Search(double[] array, double number);
    }
}
