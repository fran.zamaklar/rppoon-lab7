using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1_2
{
    class LinearSearch : SearchStrategy
    {
        public override int Search(double[] array, double number)
        {
            for(int i = 0; i < array.Length; i++)
            {
                if(array[i] == number)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
